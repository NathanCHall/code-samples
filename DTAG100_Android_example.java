//
//  DTAG100_Android_example.java
//
//  This file contains extracts from an Android app (without the distration of the user interface elements)
//  in order to demonstrate the various stages of interacting with the BLE functionality of the DTAG100 
//
//
//  Copyright (c) 2014 Dot Origin. All rights reserved.
//



public class BLEScanActivity extends Activity {
	// This value controls how many milliseconds scans last
    private static final long BLE_SCAN_PERIOD = 10000;	

    public final static String ACTION_GATT_CONNECTED =
            "com.domain.yourapp.ACTION_GATT_CONNECTED";
    public final static String ACTION_GATT_DISCONNECTED =
            "com.domain.yourapp.ACTION_GATT_DISCONNECTED";
    public final static String ACTION_GATT_SERVICES_DISCOVERED =
            "com.domain.yourapp.ACTION_GATT_SERVICES_DISCOVERED";
    public final static String ACTION_DATA_AVAILABLE =
            "com.domain.yourapp.ACTION_DATA_AVAILABLE";
    public final static String ACTION_DATA_WRITTEN =
            "com.domain.yourapp.ACTION_DATA_WRITTEN";
    public final static String ACTION_STATUS_UPDATE =
            "com.domain.yourapp.ACTION_STATUS_UPDATE";
    public final static String EXTRA_DATA_UUID =
            "com.domain.yourapp.EXTRA_UUID";
    public final static String EXTRA_DATA_VALUE =
            "com.domain.yourapp.EXTRA_DATA";

    private static final int STATE_DISCONNECTED = 0;
    private static final int STATE_CONNECTING = 1;
    private static final int STATE_CONNECTED = 2;
	
	private Handler mBleHandler;
	private Handler mProximityHandler;
    private BluetoothGatt mBluetoothGatt;
    private BluetoothManager mBluetoothManager;
	
	@Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
		
		final BluetoothManager bluetoothManager = (BluetoothManager) getSystemService(Context.BLUETOOTH_SERVICE);
		mBluetoothAdapter = bluetoothManager.getAdapter();
	}
	
    @Override
    public void onPause() {
        super.onPause();
		// Stop scanning
        scanLeDevice(false);
		// Unregister broadcast receiver and disconnect from GATT
        unregisterReceiver(mGattUpdateReceiver);
        if (mBluetoothGatt != null) {
            mBluetoothGatt.disconnect();
        }
    }
	
    @Override
    public void onResume() {
        super.onResume();
		// Register broadcast receiver and start scanning if wanted
		registerReceiver(mGattUpdateReceiver, makeGattUpdateIntentFilter());
        if (!mScanning && bleImageRes != 0) {
            scanLeDevice(true);
        }
    }
	
	// This function start BLE scanning and is the entry point to communicating with the DTAG100
	private void scanLeDevice(final boolean enable) {
        if (enable) {
			mBleHandler = new Handler();
			// Stops scanning after a pre-defined scan period.
			mBleHandler.postDelayed(new Runnable() {
				@Override
				public void run() {
					if (mScanning) {
						mScanning = false;
						gStopScanOnce = true;
						mBluetoothAdapter.stopLeScan(mLeScanCallback);
					}
				}
			}, BLE_SCAN_PERIOD);
            mBluetoothAdapter.startLeScan(mLeScanCallback);
        } else { 
            mBluetoothAdapter.stopLeScan(mLeScanCallback);
			// Here you may want to handle any action after you've discovered a known beacon, but no connected to GATT
			// be that update the UI or continue scanning
        }
    }
	
	// This is a simplified version of the BLE device callback from our demo app. We start by examining the scanRecord to see if
	// it is using Apple's iBeacon data format, and then comparing that to any specific beacons our application wants to talk to.
	// Called multiple times a second if there are any beacons in range while scanning.
    private BluetoothAdapter.LeScanCallback mLeScanCallback =
            new BluetoothAdapter.LeScanCallback() {
        @Override
        public void onLeScan(final BluetoothDevice device, final int rssi, final byte[] scanRecord) {
            runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    int type = device.getType();
                    // BLE devices including the DTAG100 should return a scan record consisting of 31 bytes of advertising data and 31 bytes of scan data
                    if (scanRecord.length == 62) {
                        BleDevice bleDevice = new BleDevice();
                        bleDevice.setLocalName(device.getName());
                        bleDevice.setAddress(device.getAddress());
                        if (!mBleAddressList.contains(bleDevice.getAddress())) {
                            int flagsLen = scanRecord[0];
                            int manufacturerLen = scanRecord[flagsLen + 1];
                            // The following is to check for beacons with custom data according to Apple specification
                            if (scanRecord[0] == (byte)0x02 &&      // Data length: 2 bytes
                                    scanRecord[1] == (byte)0x01 &&  // Data type: flags
                                    scanRecord[2] == (byte)0x06 &&  // See Bluetooth 4.0 Core Specification, vol 3, Appendix C, 18.1
                                    scanRecord[3] == (byte)0x1a &&  // Data length: 26 bytes
                                    scanRecord[4] == (byte)0xff &&  // Data type: Manufacturer specific data
                                    scanRecord[5] == (byte)0x4c &&  // Company ID: 4c 00 is Apple
                                    scanRecord[6] == (byte)0x00 &&  //
                                    scanRecord[7] == (byte)0x02 &&  // Beacon type: 02 15 is Proximity Beacon
                                    scanRecord[8] == (byte)0x15) {  //

                                byte[] appleUUID = new byte[16];    // 16 bytes of iBeacon UUID
                                for (int i = 9; i < 25; i++) {
                                    appleUUID[i-9] = scanRecord[i];
                                }

                                bleDevice.setAppleUUID(appleUUID);
                                bleDevice.setAppleMajor(scanRecord[25] << 8 | scanRecord[26]);    // 2 bytes of iBeacon major
                                bleDevice.setAppleMinor(scanRecord[27] << 8 | scanRecord[28]);    // 2 bytes of iBeacon minor

                                bleDevice.setAdvertisedRSSI(scanRecord[29]);       // 1 byte of advertised signal strength
                                bleDevice.setActualRSSI(rssi);
                                mTheApp.latestDistances.add(calculateAccuracy(bleDevice.getAdvertisedRSSI(), rssi));
                                bleDevice.setDistanceTo(mTheApp.latestDistances.getAverage());

                                String appleUuidStr = bytesToHexString(bleDevice.getAppleUUID());

								// We now check to see if this beacon is one that we care want to act on
                                if (appleUuidStr.equalsIgnoreCase(mTheApp.bleDigitalSignage1UUID) &&
                                        bleDevice.getAppleMajor() == mTheApp.bleDigitalSignage1Major &&
                                        bleDevice.getAppleMinor() == mTheApp.bleDigitalSignage1Minor) {
                                    bleDevice.setImgResourceId(R.drawable.ds1);
                                    bleDevice.setImgFile(mTheApp.bleDigitalSignage1ImgPath);
                                    mBleDeviceList.add(bleDevice);
                                    scanLeDevice(false);
                                }
                                else if (appleUuidStr.equalsIgnoreCase(mTheApp.bleDigitalSignage2UUID) &&
                                            bleDevice.getAppleMajor() == mTheApp.bleDigitalSignage2Major &&
                                            bleDevice.getAppleMinor() == mTheApp.bleDigitalSignage2Minor) {
                                    bleDevice.setImgResourceId(R.drawable.ds2);
                                    bleDevice.setImgFile(mTheApp.bleDigitalSignage2ImgPath);
                                    mBleDeviceList.add(bleDevice);
                                    scanLeDevice(false);
                                }
                                else if (appleUuidStr.equalsIgnoreCase(mTheApp.bleDigitalSignage3UUID) &&
                                        bleDevice.getAppleMajor() == mTheApp.bleDigitalSignage3Major &&
                                        bleDevice.getAppleMinor() == mTheApp.bleDigitalSignage3Minor) {
                                    bleDevice.setImgResourceId(R.drawable.ds3);
                                    bleDevice.setImgFile(mTheApp.bleDigitalSignage3ImgPath);
                                    mBleDeviceList.add(bleDevice);
                                    scanLeDevice(false);
                                }
                                else if (appleUuidStr.equalsIgnoreCase(mTheApp.bleDigitalSignage4UUID) &&
                                        bleDevice.getAppleMajor() == mTheApp.bleDigitalSignage4Major &&
                                        bleDevice.getAppleMinor() == mTheApp.bleDigitalSignage4Minor) {
                                    bleDevice.setImgResourceId(R.drawable.ds4);
                                    bleDevice.setImgFile(mTheApp.bleDigitalSignage4ImgPath);
                                    mBleDeviceList.add(bleDevice);
                                    scanLeDevice(false);
                                }
                                else if (appleUuidStr.equalsIgnoreCase(mTheApp.bleDigitalSignage5UUID) &&
                                        bleDevice.getAppleMajor() == mTheApp.bleDigitalSignage5Major &&
                                        bleDevice.getAppleMinor() == mTheApp.bleDigitalSignage5Minor) {
                                    bleDevice.setImgResourceId(R.drawable.ds5);
                                    bleDevice.setImgFile(mTheApp.bleDigitalSignage5ImgPath);
                                    mBleDeviceList.add(bleDevice);
                                    scanLeDevice(false);
                                }
                                else if (appleUuidStr.equalsIgnoreCase(mTheApp.bleDigitalSignage6UUID) &&
                                        bleDevice.getAppleMajor() == mTheApp.bleDigitalSignage6Major &&
                                        bleDevice.getAppleMinor() == mTheApp.bleDigitalSignage6Minor) {
                                    bleDevice.setImgResourceId(R.drawable.ds6);
                                    bleDevice.setImgFile(mTheApp.bleDigitalSignage6ImgPath);
                                    mBleDeviceList.add(bleDevice);
                                    scanLeDevice(false);
                                }
                                else if (appleUuidStr.equalsIgnoreCase(mTheApp.bleDigitalSignage7UUID) &&
                                        bleDevice.getAppleMajor() == mTheApp.bleDigitalSignage7Major &&
                                        bleDevice.getAppleMinor() == mTheApp.bleDigitalSignage7Minor) {
                                    bleDevice.setImgResourceId(R.drawable.ds7);
                                    bleDevice.setImgFile(mTheApp.bleDigitalSignage7ImgPath);
                                    mBleDeviceList.add(bleDevice);
                                    scanLeDevice(false);
                                }
                                else if (appleUuidStr.equalsIgnoreCase(mTheApp.bleDigitalSignage8UUID) &&
                                        bleDevice.getAppleMajor() == mTheApp.bleDigitalSignage8Major &&
                                        bleDevice.getAppleMinor() == mTheApp.bleDigitalSignage8Minor) {
                                    bleDevice.setImgResourceId(R.drawable.ds8);
                                    bleDevice.setImgFile(mTheApp.bleDigitalSignage8ImgPath);
                                    mBleDeviceList.add(bleDevice);
                                    scanLeDevice(false);
                                }
                                else if (appleUuidStr.equalsIgnoreCase(mTheApp.bleTwoWayTapUUID) &&
                                        bleDevice.getAppleMajor() == mTheApp.bleTwoWayTapMajor &&
                                        bleDevice.getAppleMinor() == mTheApp.bleTwoWayTapMinor &&
                                        bleDevice.getLocalName().equalsIgnoreCase(mTheApp.bleTwoWayTapLocalName)) {
                                    bleDevice.setImgResourceId(R.drawable.twt);
                                    bleDevice.setImgFile(mTheApp.bleTwoWayTapImgPath);
                                    mBleDeviceList.add(bleDevice);
                                    if (bleDevice.getDistanceTo() < mTheApp.bleTwoWayTapRangeValue) {
										scanLeDevice(false);
										mProximityHandler = new Handler();
										// Stops scanning after a pre-defined scan period.
										mProximityHandler.postDelayed(new Runnable() {
											@Override
											public void run() {
												connectToGatt();
											}
										}, mTheApp.bleTwoWayTapTimeValue * 1000);
                                    }
                                }
                                else if (appleUuidStr.equalsIgnoreCase(mTheApp.bleTakeawayUUID) &&
                                        bleDevice.getAppleMajor() == mTheApp.bleTakeawayMajor &&
                                        bleDevice.getAppleMinor() == mTheApp.bleTakeawayMinor &&
                                        bleDevice.getLocalName().equalsIgnoreCase(mTheApp.bleTakeawayLocalName)) {
                                    bleDevice.setImgResourceId(R.drawable.tav);
                                    bleDevice.setImgFile(mTheApp.bleTakeawayImgPath);
                                    mBleDeviceList.add(bleDevice);
                                    if (gTimeToConnect && bleDevice.getDistanceTo() < mTheApp.bleTakeawayRangeValue) {
										scanLeDevice(false);
										mProximityHandler = new Handler();
										// Stops scanning after a pre-defined scan period.
										mProximityHandler.postDelayed(new Runnable() {
											@Override
											public void run() {
												connectToGatt();
											}
										}, mTheApp.bleTakeawayTimeValue * 1000);
                                    }
                                }
                                else {  // No recognized beacons found, keep scanning
                                    scanLeDevice(true);
                                }
                            }
                        }
                        else{
                            scanLeDevice(true);
                        }
                    }
                }
            });
        }
    };

    protected static double calculateAccuracy(int txPower, double rssi) {
        if (rssi == 0) {
            return -1.0; // if we cannot determine accuracy, return -1.
        }

        double ratio = rssi*1.0/txPower;
        if (ratio < 1.0) {
            return Math.pow(ratio,10);
        }
        else {
            double accuracy =  (0.89976)*Math.pow(ratio,7.7095) + 0.111;
            return accuracy;
        }
    }

    // Kick off the process by connecting to GATT, which in turn reads the services, then the characteristics, then writes to the
    // write characteristic
    public void connectToGatt() {
        Toast.makeText(ctx, "Connecting to device...", Toast.LENGTH_SHORT).show();
        mBluetoothGatt = mTargetBTDevice.connectGatt(ctx, false, mGattCallback);
    }

    private String bytesToHexString(byte[] src) {
        StringBuilder stringBuilder = new StringBuilder("");
        if (src == null || src.length <= 0) {
            return null;
        }

        char[] buffer = new char[2];
        for (int i = 0; i < src.length; i++) {
            buffer[0] = Character.forDigit((src[i] >>> 4) & 0x0F, 16);
            buffer[1] = Character.forDigit(src[i] & 0x0F, 16);
            System.out.println(buffer);
            stringBuilder.append(buffer);
        }

        return stringBuilder.toString();
    }
	
	// Various callback methods defined by the BLE GATT API.
	private final BluetoothGattCallback mGattCallback = new BluetoothGattCallback() {
		@Override
		public void onConnectionStateChange(BluetoothGatt gatt, int status, int newState) {
			String intentAction;
			if (newState == BluetoothProfile.STATE_CONNECTED) {
				broadcastUpdate(ACTION_GATT_CONNECTED);
			}
			else if (newState == BluetoothProfile.STATE_DISCONNECTED) {
				broadcastUpdate(ACTION_GATT_DISCONNECTED);
			}
		}
		
		@Override
		// New services discovered
		public void onServicesDiscovered(BluetoothGatt gatt, int status) {
			if (status == BluetoothGatt.GATT_SUCCESS) {
				broadcastUpdate(ACTION_GATT_SERVICES_DISCOVERED);
			} else {
				Log.w(TAG, "onServicesDiscovered received: " + status);
			}
		}

		@Override
		// Result of a characteristic read operation
		public void onCharacteristicRead(BluetoothGatt gatt,
										 BluetoothGattCharacteristic characteristic,
										 int status) {
			if (status == BluetoothGatt.GATT_SUCCESS) {
				broadcastUpdate(ACTION_DATA_AVAILABLE, characteristic);
			}
			else {
				Log.i(TAG, "NO GATT_SUCCESS " + characteristic.getUuid().toString());
			}
		}

		@Override
		// Result of characteristic write operation
		public void onCharacteristicWrite (BluetoothGatt gatt, BluetoothGattCharacteristic characteristic, int status) {
			if (status == BluetoothGatt.GATT_SUCCESS) {
				broadcastUpdate(ACTION_DATA_WRITTEN, characteristic);
				Log.i(TAG, "Write return with GATT_SUCCESS!");
			}
			else {
				Log.i(TAG, "Write FAILED to return GATT_SUCCESS ");
			}
		}
	};
			
	// Update UIs here or proceed with other GATT operations
    private final BroadcastReceiver mGattUpdateReceiver = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            final String action = intent.getAction();
            if (ACTION_GATT_CONNECTED.equals(action)) {
				mConnectionState = STATE_CONNECTED;
				// Once connected, discover services
				Toast.makeText(ctx, "Connected, discovering services...", Toast.LENGTH_SHORT).show();
				mBluetoothGatt.discoverServices();
            } else if (ACTION_GATT_DISCONNECTED.equals(action)) {
                mConnectionState = STATE_DISCONNECTED;
				// You'll probably want to update your UI at this point with results from your connection
				Toast.makeText(ctx, "Connected, discovering services...", Toast.LENGTH_SHORT).show();
            } else if (ACTION_GATT_SERVICES_DISCOVERED.equals(action)) {
				// Here we get the DTAG100's service for reading and writing
                mOurDataService = mBluetoothGatt.getService(UUID.fromString("46f582f2-b45c-44f2-9723-dbbdc9b07f70"));
                if (mOurDataService != null) {
					// Here we get the first read-only characteristic of the DTAG100, which holds the first 32 bytes of the readAtt value from bledata.txt
                    BluetoothGattCharacteristic ourReadCharacteristic = mOurDataService.getCharacteristic(UUID.fromString("46f582f2-b45c-44f2-9723-dbbdc9b07f71"));
                    if (ourReadCharacteristic != null) {
                        Toast.makeText(ctx, "Reading characteristic...", Toast.LENGTH_SHORT).show();
                        mBluetoothGatt.readCharacteristic(ourReadCharacteristic);
                    }
                }
            } else if (ACTION_DATA_AVAILABLE.equals(action)) {
				// Display the value that we read
                Toast.makeText(ctx, intent.getStringExtra(MainActivity.EXTRA_DATA_VALUE), Toast.LENGTH_SHORT).show();
				// After reading a value, you might also want to write back to the DTAG100.
                BluetoothGattCharacteristic ourWriteCharacteristic = mOurDataService.getCharacteristic(UUID.fromString("46f582f2-b45c-44f2-9723-dbbdc9b07f81"));
                if (ourWriteCharacteristic != null) {
                    ourWriteCharacteristic.setValue("Hello DTAG100");
                    Toast.makeText(ctx, "Writing characteristic...", Toast.LENGTH_SHORT).show();
                    mBluetoothGatt.writeCharacteristic(ourWriteCharacteristic);
                }
            }
            else if (ACTION_DATA_WRITTEN.equals(action)) {
                // All done, now disconnect                                                                                         
                Toast.makeText(ctx, "Disconnecting", Toast.LENGTH_SHORT).show();
                mBluetoothGatt.disconnect();
            }
            else if (ACTION_STATUS_UPDATE.equals(action)) {
				// Used to just provide status updates
                String status = intent.getStringExtra(EXTRA_DATA_VALUE);
                Toast.makeText(ctx, status, Toast.LENGTH_SHORT).show();
            }
        }
    };

    private void broadcastUpdate(final String action) {
        final Intent intent = new Intent(action);
        sendBroadcast(intent);
    }

    private void broadcastUpdate(final String action, final String status) {
        final Intent intent = new Intent(action);
        intent.putExtra(EXTRA_DATA_VALUE, status);
        sendBroadcast(intent);
    }

    private void broadcastUpdate(final String action,
                                 final BluetoothGattCharacteristic characteristic) {
        final Intent intent = new Intent(action);

        final byte[] data = characteristic.getValue();
        if (data != null && data.length > 0) {
            intent.putExtra(EXTRA_DATA_UUID, characteristic.getUuid().toString());
            intent.putExtra(EXTRA_DATA_VALUE, new String(data));
        }
        sendBroadcast(intent);
    }

	// This is the filter for the broadcast receiver
    private static IntentFilter makeGattUpdateIntentFilter() {
        final IntentFilter intentFilter = new IntentFilter();
        intentFilter.addAction(ACTION_GATT_CONNECTED);
        intentFilter.addAction(ACTION_GATT_DISCONNECTED);
        intentFilter.addAction(ACTION_GATT_SERVICES_DISCOVERED);
        intentFilter.addAction(ACTION_DATA_AVAILABLE);
        intentFilter.addAction(ACTION_DATA_WRITTEN);
        intentFilter.addAction(ACTION_STATUS_UPDATE);
        return intentFilter;
    }
}
	
	
public class TheApp extends Application {

    public final static int WRITEBACK_EMAIL = 1;
    public final static int WRITEBACK_CUSTOM = 2;
    public final static int WRITEBACK_NONE = 3;

    private static Context context;

    public RollingAverage latestDistances = new RollingAverage(20);

    public class RollingAverage {
        private double[] latest;
        private int index = 0;

        public RollingAverage(int size) {
            latest = new double[size];
        }

        public void add(double value) {
            if (index < latest.length) {
                latest[index] = value;
                index++;
            }
            else {
                index = 0;
                latest[index] = value;
            }
        }

        public double getAverage() {
            double sum = 0;
            int count = 0;
            for (double value : latest) {
                if (value != 0) {
                    sum += value;
                    if (count < latest.length) {
                        count++;
                    }
                }
            }
            double average = sum / count;

            return average;
        }
    }

    public void onCreate(){
        super.onCreate();
        TheApp.context = getApplicationContext();
    }

    public static Context getAppContext() {
        return TheApp.context;
    }
}


public class BleDevice {
    private String localName = "";
    private String address = "";
    private byte[] manufacturerID = new byte[2];
    private byte[] appleUUID = new byte[16];
    private int appleMajor = 0;
    private int appleMinor = 0;
    private int advertisedRSSI = 0;
    private int actualRSSI = 0;
    private int resId = 0;
    private double distanceTo = 0;
    private String imgFile = "";

    public String getLocalName() {
        return this.localName;
    }

    public String getAddress() {
        return this.address;
    }

    public byte[] getAppleUUID() {
        return this.appleUUID;
    }

    public int getAppleMajor() {
        return this.appleMajor;
    }

    public int getAppleMinor() {
        return this.appleMinor;
    }

    public int getAdvertisedRSSI() {
        return this.advertisedRSSI;
    }

    public int getActualRSSI() {
        return this.actualRSSI;
    }

    public int getImgResourceId() { return this.resId;}

    public double getDistanceTo() { return this.distanceTo;}

    public String getImgFile() { return this.imgFile;}

    public void setLocalName(String text) {
        this.localName = text;
    }

    public void setAddress(String text) {
        this.address = text;
    }

    public void setAppleUUID(byte[] uuid) {
        this.appleUUID = uuid;
    }

    public void setAppleMajor(int major) {
        this.appleMajor = major;
    }

    public void setAppleMinor(int minor) {
        this.appleMinor = minor;
    }

    public void setAdvertisedRSSI(int rssi) { this.advertisedRSSI = rssi; }

    public void setActualRSSI(int rssi) { this.actualRSSI = rssi; }

    public void setImgResourceId(int res) { this.resId = res;}

    public void setDistanceTo(double distance) { this.distanceTo = distance;}

    public void setImgFile(String imgFile) { this.imgFile = imgFile;}
}
